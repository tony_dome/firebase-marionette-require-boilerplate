define(['jquery', 'backbone', 'marionette', 'underscore', 'handlebars', 'firebase', 'backbone-firebase', 'firebase-simple-login'],
    function ($, Backbone, Marionette, _, Handlebars, Firebase, Backfire, FirebaseSimpleLogin) {
        var App = new Backbone.Marionette.Application();

        function isMobile() {
            var userAgent = navigator.userAgent || navigator.vendor || window.opera;
            return ((/iPhone|iPod|iPad|Android|BlackBerry|Opera Mini|IEMobile/).test(userAgent));
        }

        //Organize Application into regions corresponding to DOM elements
        //Regions can contain views, Layouts, or subregions nested as necessary
        App.addRegions({
            headerRegion:"header",
            mainRegion:"#main"
        });

        App.addInitializer(function () {
            Backbone.history.start();
        });

        App.mobile = isMobile();

        //Firebase ref
        App.firebase = new Firebase('https://backbone-test.firebaseio-demo.com');
        
        //Backfire
        var 
          FireIndex = Backbone.Firebase.Collection.extend({
            model     : Backbone.Model,
            firebase  : new Firebase('https://backbone-test.firebaseio-demo.com/index')
          }),
          fbIndex = new FireIndex();
        
        //Firebase Simple Login - Anant owns the account at 'https://backbone-test.firebaseio-demo.com/'
        //Let him know to enable Simple Login for email, etc.

        var onStateChanged = function (err, user) {
          var that = this;
          if (err) {
            console.log('error in authentication');
          } else if (user) {
            console.log('user is found');
          } else {
            console.log('user is logged out');
          }
        };
        
        // App.auth = new FirebaseSimpleLogin(App.firebase, onStateChanged);

        return App;
    });
